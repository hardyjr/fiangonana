import { getRepository ,  getConnection, DeleteResult } from 'typeorm';
import { Site } from '../entity/Site';


class SiteService {
    async getList(): Promise<Partial<Site>[]> {        
        return  await getRepository(Site).find();
    }

    async getById(id: number): Promise<Site | undefined> {
        try {
            return await getRepository(Site).findOne({ id });
        } catch (e) {
            return undefined;
        }
    }

    async create(item: Site): Promise<Site> {
        let newSite = new Site();
        newSite = item;
        return await getRepository(Site).save(newSite);
    }

    async update(id: number, item: Site): Promise<Site> {
        let site = await getRepository(Site).findOne({id})
        site = {...site, ...item};

        return await getRepository(Site).save({...site});
    }
    async delete(id: number): Promise<DeleteResult> {
        return await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Site)
            .where('id = :id', { id })
            .execute();

    }
}

export const siteService = new SiteService();
