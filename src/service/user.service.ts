import { getRepository } from 'typeorm';
import { User } from '../entity/User';
import bcrypt from 'bcryptjs';


class UserService {
    async getList(): Promise<Partial<User>[]> {
       return  [];
    }

    async getById(id: number): Promise<User | undefined> {
        try {
            return await getRepository(User).findOne({ id });
          } catch (e) {
            return  undefined;
          }
    }

    async create(item: User): Promise<User> {
        let newUser = new User();
        if (item.password) {
            item.password = await bcrypt.hashSync(item.password, 10);
        }
        newUser = item;

        return await getRepository(User).save(newUser);
    }

    async getHash(password: string): Promise<string> {
        return bcrypt.hash(password, 10);
    }

    async compareHash(password: string, hash: string): Promise<boolean> {
      return bcrypt.compare(password, hash);
    }

}

export const userService = new UserService();
