import { getRepository, getConnection, DeleteResult } from 'typeorm';
import { Pilgrim } from '../entity/Pilgrim';
const notImplemented = 'Method not implemented.';

class PilgrimService {
    async getList(): Promise<Partial<Pilgrim>[]> {
        return  await getRepository(Pilgrim).find();
    }

    async getById(id: number): Promise<Pilgrim | undefined> {
        return await getRepository(Pilgrim).findOne({ id });
    }

    async create(item: Pilgrim): Promise<Pilgrim> {
        let newPillgrim = new Pilgrim();
        newPillgrim = item;

        return await getRepository(Pilgrim).save(newPillgrim);
    }
    async update(id: number, item: Pilgrim): Promise<Pilgrim> {
        let pilgrim = await getRepository(Pilgrim).findOne({id})
        pilgrim = {...pilgrim, ...item};

        return await getRepository(Pilgrim).save({...pilgrim});
    }
    async delete(id: number): Promise<DeleteResult> {
        return await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Pilgrim)
            .where('id = :id', { id })
            .execute();

    }

}

export const pilgrimService = new PilgrimService();
