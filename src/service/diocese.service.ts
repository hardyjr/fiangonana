import { getRepository ,  getConnection, DeleteResult } from 'typeorm';
import { Diocese } from '../entity/Diocese';


class DioceseService {
    async getList(): Promise<Partial<Diocese>[]> {        
        return  await getRepository(Diocese).find();
    }

    async getById(id: number): Promise<Diocese | undefined> {
        try {
            return await getRepository(Diocese).findOne({ id });
        } catch (e) {
            return undefined;
        }
    }

    async create(item: Diocese): Promise<Diocese> {
        let newDiocese = new Diocese();
        newDiocese = item;
        return await getRepository(Diocese).save(newDiocese);
    }

    async update(id: number, item: Diocese): Promise<Diocese> {
        let diocese = await getRepository(Diocese).findOne({id})
        diocese = {...diocese, ...item};

        return await getRepository(Diocese).save({...diocese});
    }
    async delete(id: number): Promise<DeleteResult> {
        return await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Diocese)
            .where('id = :id', { id })
            .execute();

    }
}

export const dioceseService = new DioceseService();
