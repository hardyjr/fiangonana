import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import passport from 'passport';

import { config } from '../config/app.config';

import { AuthenticationResponse } from '../model/authentication.model';

class AuthenticationService {
    logIn(req: Request, res: Response): Promise<AuthenticationResponse> {
        return new Promise((resolve, reject) => {
            passport.authenticate('local', { session: false }, (err, retUser, info) => {
                if (err || !retUser) {
                    reject(err || info);
                } else {
                    const user = {
                        id: retUser.id, email: retUser.email, adresse: retUser.adresse,
                        firstname: retUser.firstname, lastname: retUser.lastname, role: retUser.role
                    };
                    const token = jwt.sign(user, config.jwt.secretKey, {
                        expiresIn: config.jwt.expiration
                    });
                    resolve({ user, token });
                }
            })(req, res);
        });
    }

    async getUserBytoken(req: Request, res: Response): Promise<any> {
        const authorization = req && req.headers && req.headers.authorization && req.headers.authorization.split(' ')[1] || '';
        let decoded: any;
        try {
            decoded = jwt.verify(authorization, config.jwt.secretKey);
        } catch (e) {

        }
        return new Promise((resolve, reject) => {
            const user = {
                id: decoded.id, email: decoded.email, adresse: decoded.adresse,
                firstname: decoded.firstname, lastname: decoded.lastname, role: decoded.role
            };
            const token = jwt.sign(user, config.jwt.secretKey, {
                expiresIn: config.jwt.expiration
            });
            resolve({ user, token });

        })




    }


}

export const authenticationService = new AuthenticationService();
