import { Request, Response, NextFunction } from 'express';
import  multer from 'multer';

class UploadController {
    add(req: Request, res: Response){
        
        return res.status(200).json({ image: req.file.filename });
    }
}

export const uploadController = new UploadController();
