import { Request, Response } from 'express';
import { dioceseService } from '../service/diocese.service';
import { Diocese } from '../entity/Diocese';

const notImplemented = 'Method not implemented.';

class DioceseController {
    getList(req: Request, res: Response): void {
        dioceseService
            .getList()
            .then((response: Partial<Diocese>[]) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    getById(req: Request, res: Response): void {
        dioceseService
            .getById(Number(req.params.id))
            .then((response: Diocese | undefined) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    create(req: Request, res: Response): void {
        dioceseService
            .create(req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    update(req: Request, res: Response): void {
        const { id } = req.params;
        dioceseService
            .update(Number(id), req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    delete(req: Request, res: Response): void {
        const { id } = req.params;
        dioceseService
            .delete(Number(id))
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }
}

export const dioceseController = new DioceseController();