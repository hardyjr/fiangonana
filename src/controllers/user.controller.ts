import { Request, Response } from 'express';
import { userService } from '../service/user.service';
import { User } from '../entity/User';

const notImplemented = 'Method not implemented.';

class UserController {
    getList(req: Request, res: Response): void {
        userService
            .getList()
            .then((response: Partial<User>[]) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    getById(req: Request, res: Response): void {
        userService
            .getById(Number(req.params.id))
            .then((response: User | undefined) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    create(req: Request, res: Response): void {
        userService
            .create(req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }
}

export const userController = new UserController();
