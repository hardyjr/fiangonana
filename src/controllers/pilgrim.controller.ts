import { Request, Response } from 'express';
import { pilgrimService } from '../service/pilgrim.service';
import { Pilgrim } from '../entity/Pilgrim';

const notImplemented = 'Method not implemented.';

class PilgrimController {
    getList(req: Request, res: Response): void {
        pilgrimService
            .getList()
            .then((response: Partial<Pilgrim>[]) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    getById(req: Request, res: Response): void {
        pilgrimService
            .getById(Number(req.params.id))
            .then((response: Pilgrim | undefined) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    create(req: Request, res: Response): void {
        pilgrimService
            .create(req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }
    update(req: Request, res: Response): void {
        const { id } = req.params;
        pilgrimService
            .update(Number(id), req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    delete(req: Request, res: Response): void {
        const { id } = req.params;
        pilgrimService
            .delete(Number(id))
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }
}

export const pilgrimController = new PilgrimController();
