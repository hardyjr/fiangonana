import { Request, Response } from 'express';
import { siteService } from '../service/site.service';
import { Site } from '../entity/Site';

const notImplemented = 'Method not implemented.';

class SiteController {
    getList(req: Request, res: Response): void {
        siteService
            .getList()
            .then((response: Partial<Site>[]) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    getById(req: Request, res: Response): void {
        siteService
            .getById(Number(req.params.id))
            .then((response: Site | undefined) => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    create(req: Request, res: Response): void {
        siteService
            .create(req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    update(req: Request, res: Response): void {
        const { id } = req.params;
        siteService
            .update(Number(id), req.body)
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }

    delete(req: Request, res: Response): void {
        const { id } = req.params;
        siteService
            .delete(Number(id))
            .then(response => res.status(200).json(response))
            .catch(err => res.status(500).json(err));
    }
}

export const siteController = new SiteController();