import { Router } from 'express';
import { pilgrimController } from '../controllers/pilgrim.controller';
import  passport from 'passport';
class PilgrimRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    private init() {
        this.router.route('/').get(passport.authenticate('jwt', { session: false }), pilgrimController.getList);
        this.router.route('/:id').get(passport.authenticate('jwt', { session: false }), pilgrimController.getById);
        this.router.route('/').post(passport.authenticate('jwt', { session: false }), pilgrimController.create);
        this.router.route('/:id').put(passport.authenticate('jwt', { session: false }), pilgrimController.update);
        this.router.route('/:id').delete(passport.authenticate('jwt', { session: false }), pilgrimController.delete);

    }
}

const pilgrimRouter = new PilgrimRouter();

export const pilgrimRoutes = pilgrimRouter.router;
