import { Router } from 'express';
import { siteController } from '../controllers/site.controller';
import  passport from 'passport';
class SiteRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    private init() {
        this.router.route('/').get(passport.authenticate('jwt', { session: false }), siteController.getList);
        this.router.route('/:id').get(passport.authenticate('jwt', { session: false }), siteController.getById);
        this.router.route('/').post(passport.authenticate('jwt', { session: false }), siteController.create);
        this.router.route('/:id').put(passport.authenticate('jwt', { session: false }), siteController.update);
        this.router.route('/:id').delete(passport.authenticate('jwt', { session: false }), siteController.delete);

    }
}

const siteRouter = new SiteRouter();

export const siteRoutes = siteRouter.router;
