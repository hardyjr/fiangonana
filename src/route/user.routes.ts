import { Router } from 'express';
import { userController } from '../controllers/user.controller';
import  passport from 'passport';
class UserRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    private init() {
        this.router.route('/').get(userController.getList);
        this.router.route('/token').get(passport.authenticate('jwt', { session: false }), userController.getList);
        this.router.route('/:id').get(userController.getById);
        this.router.route('/').post(userController.create);
    }
}

const userRouter = new UserRouter();

export const userRoutes = userRouter.router;
