import { Router } from 'express';

import { userRoutes } from './user.routes';
import { authenticationRoutes } from './authentication.routes';
import { pilgrimRoutes } from './pilgrim.route';
import { dioceseRoutes } from './diocese.route';
import { siteRoutes } from './site.route';
import { uploadRoutes } from './upload.route';

class AppRouter {
    router: Router;
    constructor() {
        this.router = Router();
        this.init();
    }

    init() {
        this.router.get('/api-status', (req, res) =>
            res.json({ status: 'API is OK' })
        );
        this.router.use('/user', userRoutes);
        this.router.use('/authentication', authenticationRoutes);
        this.router.use('/pilgrim', pilgrimRoutes);
        this.router.use('/diocese', dioceseRoutes);
        this.router.use('/site', siteRoutes);
        this.router.use('/upload', uploadRoutes);
    }
}

const appRouter = new AppRouter();
export const appRoutes = appRouter.router;
