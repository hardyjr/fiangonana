import { Router } from 'express';
import { dioceseController } from '../controllers/diocese.controller';
import  passport from 'passport';
class DioceseRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    private init() {
        this.router.route('/').get(passport.authenticate('jwt', { session: false }), dioceseController.getList);
        this.router.route('/:id').get(passport.authenticate('jwt', { session: false }), dioceseController.getById);
        this.router.route('/').post(passport.authenticate('jwt', { session: false }), dioceseController.create);
        this.router.route('/:id').put(passport.authenticate('jwt', { session: false }), dioceseController.update);
        this.router.route('/:id').delete(passport.authenticate('jwt', { session: false }), dioceseController.delete);

    }
}

const dioceseRouter = new DioceseRouter();

export const dioceseRoutes = dioceseRouter.router;
