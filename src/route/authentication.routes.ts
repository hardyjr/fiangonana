import { Router } from 'express';
import { authenticationController } from '../controllers/authentication.controller';

class AuthenticationRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    private init() {
        this.router.route('/login').post(authenticationController.logIn);
        this.router.route('/token').get(authenticationController.getUserBytoken);
    }
}

const authenticationRouter = new AuthenticationRouter();

export const authenticationRoutes = authenticationRouter.router;
