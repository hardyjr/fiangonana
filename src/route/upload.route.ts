import { Router, Request, Response, NextFunction } from 'express';
import { uploadController } from '../controllers/upload.controller';
import  { uploadVisual } from '../config/upload.config';
import { logger } from '../config/app.logger';
import  passport from 'passport';

class UploadRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    private init() {
        this.router.route('/photo').post(
            passport.authenticate('jwt', { session: false }),
            uploadVisual.single('profil'),
            (err: any, req: Request, res: Response, next: NextFunction) => {
                logger.error(`Error on uploading photo: ${err.stack}`);
                next(err);
            },
            uploadController.add
        );
    }
}

const uploadRouter = new UploadRouter();

export const uploadRoutes = uploadRouter.router;
