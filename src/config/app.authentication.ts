import  passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';

import { config } from './app.config';
import { getRepository } from 'typeorm';
import { Request } from 'express';
import { User } from '../entity/User';
import { userService } from '../service/user.service';

const localStrategy = new LocalStrategy(
    { usernameField: 'email' },
    async (email, password, done) => {
        try {

            const user = await getRepository(User).findOne({ email });
            if (!user) {
                return done(null, false, { message: 'Aucun compte correspondant. Veuillez vous inscrire.' });
            }

            const validPassword = await userService.compareHash(password, user.password);
            if (!validPassword) {
                return done(null, false, { message: 'Mot de passe invalide' });
            }

            return done(null, user);
        } catch (error) {
            return done(error);
        }
    }
);

const extractTokenFromParams = (req: Request) => (req.params && req.params.token) || null;

const jwtStrategy = new JwtStrategy(
    {
        secretOrKey: config.jwt.secretKey,
        jwtFromRequest: ExtractJwt.fromExtractors([
            extractTokenFromParams,
            ExtractJwt.fromAuthHeaderAsBearerToken()
        ])
    },
    async (jwtPayload, done) => {
        try {
            const user = await getRepository(User).findOne({id: jwtPayload.id});

            return user ? done(null, user) : done(null, false);
        } catch (error) {
            return done(error);
        }
    }
);

passport.use(localStrategy);
passport.use(jwtStrategy);

export default passport;
