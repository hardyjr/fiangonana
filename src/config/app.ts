import bodyParser from 'body-parser';
import  express from 'express';
import  helmet from 'helmet';
import  morgan from 'morgan';
import { Server } from 'http';
import appRoot from 'app-root-path';
import  cors from 'cors';
import swaggerUi  from 'swagger-ui-express';
import swaggerDocument from '../swagger.json';

import passport from './app.authentication';
import { logger, morganStream } from './app.logger';
import { appRoutes } from '../route/app.routes';

export class App {
  private app: express.Application;

  constructor() {
    this.app = express();
  }

  public init(port: number): Server {
    this.initMiddlewares();
    this.initRoutes();
    this.initStatics();

    return this.app.listen(port, () => {
      logger.info(`app started, listening on port ${port}`);
    });
  }

  private initMiddlewares() {
    this.app.use(cors());
    this.app.use(passport.initialize());
    this.app.use(helmet());
    this.app.use(bodyParser.json({limit:'50mb'}));
    this.app.use(bodyParser.urlencoded({ extended: true, limit:'50mb' }));
    this.app.use(morgan('combined', { stream: morganStream }));
    this.app.use('/apidoc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));   
  }

  private initRoutes() {
    this.app.use('/api/v1', appRoutes);
  }

  private initStatics() {
    this.app.use(express.static(`${appRoot}/medias`));
    this.app.use(express.static(`${appRoot}/public`));
  }
}

export const app = new App();
