import  multer from 'multer';
import  path from 'path';
import { logger } from './app.logger';

/**CONFIG VISUALS */
const storageVisual = multer.diskStorage({
    destination:  (req, file, cb) => {
    cb(null, './medias/')
  },
  filename:  (req, file, cb) => {
    cb(null, Date.now() + '-' +file.originalname )
  }
})

export const uploadVisual = multer({
    storage: storageVisual,
    fileFilter: (req, file, cb) => {
        const types = /jpeg|jpg|png/;
        const extensionName = types.test(path.extname(file.originalname).toLowerCase());
        if (extensionName) {
            cb(null, true);
        } else {
            cb(null, false);
        }
    }
});
