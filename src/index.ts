import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { app } from './config/app';

import { logger } from './config/app.logger';
import { config } from './config/app.config';

logger.info('Starting application ...');

createConnection().then(() => {
    const srv = app.init(config.server.port);
}).catch((error) => {
    logger.info(`Database connection failed with error ${error}`);
});
