import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Diocese {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id!: number;

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
    })
    name!: string;

    @Column({
        type: "varchar",
        length: 150,
    })
    patron!: string;

    @Column({
        type: "varchar",
        length: 150,
    })
    bishop!: number;

    @Column({
        type: "int",
        nullable: true
    })
    NumberOfChurch!: number;

    @Column({
        type: "int",
        nullable: true
    })
    NumberOfChretien!: number;

}
