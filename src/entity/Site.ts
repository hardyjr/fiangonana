import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Site {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id!: number;

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
    })
    name!: string;

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
    })
    addresse!: string;

    @Column({
        type: "int"
    })
    numberSalle!: number;

    @Column({
        type: "int"
    })
    Capacity!: number;

}
