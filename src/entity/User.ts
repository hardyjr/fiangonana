import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id!: number;

    @Column({
        type: "varchar",
        length: 150,
        unique: true,
    })
    email!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    lastname!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    firstname!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    adresse!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    password!: string;
}
