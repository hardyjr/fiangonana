import { Entity, Column, PrimaryGeneratedColumn, Generated } from 'typeorm';


@Entity()
export class Pilgrim {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id!: number;

    @Column()
    @Generated("uuid")
    uuid!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    sexe!: String;

    @Column({
        type: "varchar",
        length: 150
    })
    email!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    lastname!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    firstname!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    phonenumber!: string;

    @Column({
        type: "timestamp",
    })
    dateofbirth!: Date;

    @Column({
        type: "varchar",
        length: 150
    })
    adresse!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    apv!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    faritra!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    church!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    missionarydistric!: string;

    @Column({
        type: "varchar",
        length: 150
    })
    diocese!: string;

    @Column({

        type: "varchar",
        length: 150,
        nullable: true
    })
    commissioner!: string;

    @Column({
        type: "varchar",
        length: 150,
        nullable: true
    })
    movement!: string;

    @Column({
        type: "boolean",
        default: false,
    })
    bapteme!: boolean;

    @Column({
        type: "boolean",
        default: false,
    })
    confirmation!: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    married!: boolean;
    @Column({
        type: "boolean",
        default: false
    })
    isReligious!: boolean;

    @Column({
        type: "varchar",
        length: 150,
        nullable: true
    })
    photo!: string;
    @Column({
        type: "timestamp",
        default: () => 'CURRENT_TIMESTAMP'
    })
    createddate!: Date;
    @Column({
        type: "timestamp",
        nullable: true,
    })
    deletedate!: Date;
}
